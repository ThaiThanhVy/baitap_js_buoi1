/**
 * Input
 * Chiều dài = 5
 * Chiều rộng = 3
 * 
 * Todo
 * Diện tích = chiều dài * chiều rộng
 * Chu vi = (chiều dài + chiều rông) * 2
 * 
 * output
 * Diện tích = 15
 * Chu vi = 16
 * 
 * 
 */

var chieuDai = 5;
var chieuRong = 3;

dienTich = chieuDai * chieuRong;
chuVi = (chieuDai + chieuRong) * 2;

console.log("dienTich ", dienTich);
console.log("chuVi ", chuVi);