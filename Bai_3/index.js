/**
 * Input
 * 1usd = 23.500vnd
 * Cho người dùng nhập vào số tiền usd bất kì
 * 
 * Todo
 * Tạo ra 1 ô input và button
 * Cho button 1 cái onclick 
 * Công thức đổi là lấy tiền việt * tiền usd
 * Thêm thẻ p để in ra màn hình
 * 
 * Output
 * In ra màn hình số tiền đã đổi
 * 
 */

function doi() {
    var tienViet = 23500;
    var tienUSD = document.getElementById("txt-tien-usd").value * 1;

    console.log(tienViet , tienUSD);
    result = tienViet * tienUSD;

    document.getElementById("result").innerHTML = `<h1>${result} VND</h1>`;
}